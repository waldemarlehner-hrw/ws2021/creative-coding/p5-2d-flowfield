import p5, { Vector } from "p5";

class Boid {
    
    private directionAndMagnitude : Vector = new Vector();

    constructor(private position: Vector, private maxSpeed: number, private canvasSize: number){}

    public act(p: p5, force: Vector) {

        this.directionAndMagnitude = Vector.add(this.directionAndMagnitude, force);
        console.log(this.directionAndMagnitude.x, this.directionAndMagnitude.y, force.x, force.y)
        if(this.directionAndMagnitude.mag() > this.maxSpeed) {
            this.directionAndMagnitude = Vector.mult(this.directionAndMagnitude.normalize(), this.maxSpeed)
        }

        this.position = Vector.add(this.position, this.directionAndMagnitude);

        if(this.position.x < 0) {
            this.position.x += this.canvasSize;
        }
        else if(this.position.x > this.canvasSize) {
            this.position.x %= this.canvasSize;
        }
        if(this.position.y < 0) {
            this.position.y += this.canvasSize;
        }
        else if(this.position.y > this.canvasSize) {
            this.position.y %= this.canvasSize;
        }


        p.push()
        p.noStroke()
        p.fill("green")
        p.ellipse(this.position.x, this.position.y, 5, 5);
        p.stroke("green")
        const unit = this.directionAndMagnitude.copy().normalize()
        const lookAtLineEnd = Vector.add(this.position, Vector.mult(unit, 10))
        p.line(this.position.x, this.position.y, lookAtLineEnd.x, lookAtLineEnd.y)
        p.pop()
    }

    public get Position() {
        return {x: this.position.x, y: this.position.y}
    }


}

export default Boid;