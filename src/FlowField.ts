import {Vector} from "p5";

class FlowField {
    private obstacles: {x: number, y: number, f: number}[] = [];
    private target!: Vector;

    private magnitudeClamp = 1;

    public setTargetPosition(x: number, y: number) {
        const vec = new Vector();
        vec.set(x,y,0);
        this.target = vec;
    }

    public addObstacle(point: {x: number, y: number}, force: number) {
        this.obstacles.push({x: point.x, y: point.y, f: force});
    }

    public clearObstacles() {
        this.obstacles = [];
    }

    public getAllObstacles() {
        return [...this.obstacles];
    }

    public Evaluate(position: {x: number, y: number}) {
        const vec = new Vector().set(position.x, position.y, 0);

        const evaluatedObstacleForces = this.obstacles.map( e => {
            const position = new Vector().set(e.x, e.y, 0);
            const direction = Vector.sub(position, vec);
            const directionLength = direction.mag();
            const force = this.distanceToObstacleToForceFunction(directionLength) * e.f;
            
            const normalizedDirection = direction.normalize();
            return Vector.mult(normalizedDirection, -force);
        });

        if(evaluatedObstacleForces.length === 0) {
            evaluatedObstacleForces.push(new Vector())
        }
        const obstacleForceSum = evaluatedObstacleForces.reduce( (prev: Vector, current: Vector) => {
            return Vector.add(prev, current);
        })
    
        const targetDirection = Vector.sub(this.target, vec).normalize()
        let flowDirectionAndMagnitude = Vector.add(obstacleForceSum, targetDirection);
        if(flowDirectionAndMagnitude.mag() > this.magnitudeClamp) {
            const multiplier = this.magnitudeClamp / flowDirectionAndMagnitude.mag();
            flowDirectionAndMagnitude = Vector.mult(flowDirectionAndMagnitude, multiplier); 
        }

        return {
            from: vec,
            to: Vector.add(vec, flowDirectionAndMagnitude),
            flow: flowDirectionAndMagnitude
        }
    }


    // https://www.desmos.com/calculator/eh84u9fs51?lang=de
    private distanceToObstacleToForceFunction(distance: number) {
        if(distance <= 1) {
            return 1;
        }
        if(distance > 40) {
            return 0;
        }
        return 0.07/Math.sqrt(distance);
    }
}

export default FlowField;