import p5 from "p5";
import Boid from "./Boid";
import FlowField from "./FlowField";

class FlowfieldSketch {
	
	private canvas?: p5.Renderer;

	private screenSize = 500;
	private indicatorDensity = 10;
	private indicatorLength = 5;

	private readonly flowfield = new FlowField();

	private readonly boids: Boid[] = [];
	
	constructor(private p: p5){
		document.oncontextmenu = (): false | undefined => {
			if(p.mouseX <= p.width && p.mouseX >= 0 && p.mouseY <= p.height && p.mouseY >= 0) {
				return false;
			}
			return undefined;
		}
		p.setup = () => this.setup();
		p.draw = () => this.draw();
		p.mouseClicked = () => this.mouseClicked();
	}

	private mouseClicked() {
		if(this.p.mouseButton === this.p.LEFT) {
			if(this.p.mouseX > 0 && this.p.mouseX < this.p.width && this.p.mouseY > 0 && this.p.mouseY < this.p.height) {
				this.flowfield.addObstacle({x: this.p.mouseX, y: this.p.mouseY}, 100);
			}
			else {
				this.flowfield.clearObstacles()
			}
		}
	}

	private setup() {
		this.canvas = this.p.createCanvas(this.screenSize, this.screenSize);
		this.canvas.parent("app");
		this.p.noStroke()
		this.boids.push(new Boid(new p5.Vector().set(this.screenSize/2, this.screenSize/2), 5, this.screenSize))
	}

	private draw() {
		
		this.p.background("#eaeaea");
		this.flowfield.setTargetPosition(this.p.mouseX, this.p.mouseY);
		this.p.fill("black");
		const obstacles = this.flowfield.getAllObstacles();
		for(const obstacle of obstacles) {
			this.p.ellipse(obstacle.x, obstacle.y, obstacle.f / 10, obstacle.f / 10);
		}
		this.p.fill("red")
		const indicatorCount = Math.floor(this.screenSize/this.indicatorDensity);
		for(let x = 0; x < indicatorCount; x++) {
			for(let y = 0; y < indicatorCount; y++) {
				const origin = {x: (x+.5) * this.indicatorDensity, y: (y+.5)* this.indicatorDensity}
				this.p.noStroke()
				this.p.ellipse(origin.x, origin.y, 2, 2);
				const forceVector = this.flowfield.Evaluate(origin);
				this.p.stroke("red")
				const lineEnd = p5.Vector.add(forceVector.from, p5.Vector.mult(forceVector.flow, this.indicatorLength))
				this.p.line(forceVector.from.x, forceVector.from.y, lineEnd.x, lineEnd.y);
			}
		}

		for(const boid of this.boids) {
			const force = this.flowfield.Evaluate(boid.Position);
			boid.act(this.p, force.flow)
		}

	
	}
	
	public static toP5Sketch(): (p5: p5) => void {
		return (p5: p5) => new FlowfieldSketch(p5);
	}
}

export default FlowfieldSketch;
